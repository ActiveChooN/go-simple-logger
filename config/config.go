package config

import "os"

type Config struct {
	DB 		*DBConfig
	AuthUrl	string
	Host	string
}

type DBConfig struct {
	Server		string
	Database	string
}

func GetConfig() *Config {
	return &Config{
		DB: &DBConfig{
			Server: 	os.Getenv("DB_SERVER"),
			Database:	os.Getenv("DB_NAME"),
		},
		AuthUrl:	os.Getenv("AUTH_URL"),
		Host:		os.Getenv("APP_HOST"),
	}
}
