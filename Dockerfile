FROM golang:alpine as builder
RUN apk update && apk add --no-cache git curl
RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
COPY . $GOPATH/src/local/go-simple-logger/
WORKDIR $GOPATH/src/local/go-simple-logger/
RUN dep ensure
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o /go/bin/main .
FROM scratch
COPY --from=builder /go/bin/main /app/
WORKDIR /app
CMD ["./main"]