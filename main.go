package main

import (
	"local/go-simple-logger/app"
	"local/go-simple-logger/config"
)

func main() {
	config := config.GetConfig()
	loggerApp := &app.App{}
	loggerApp.Initialize(config)
	loggerApp.Run()
}
