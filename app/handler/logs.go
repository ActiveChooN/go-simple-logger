package handler

import (
	"time"
	"strconv"
	"net/http"
	"encoding/json"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"github.com/gorilla/mux"

	"local/go-simple-logger/app/model"
)



func GetLogsPlain(db *mgo.Database, w http.ResponseWriter, r *http.Request) {
	var response string
	logs, err := new(model.Log).FindLastN(db, 500)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	for _, el := range *logs{
		response += el.ToString()
	}
	respondPlain(w, http.StatusOK, response)
}

func GetLogs(db *mgo.Database, w http.ResponseWriter, r *http.Request) {
	logs, err := new(model.Log).FindLastN(db,500)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	respondJSON(w, http.StatusOK, logs)
}

func GetLogsAll(db *mgo.Database, w http.ResponseWriter, r *http.Request) {
	logs, err := new(model.Log).FindAll(db)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	respondJSON(w, http.StatusOK, logs)
}

func GetLogsByMachinePlain(db *mgo.Database, w http.ResponseWriter, r *http.Request){
	var response string
	mId, err := strconv.ParseInt(mux.Vars(r)["m_id"],10, 64)
	logs, err := new(model.Log).FindByMachineLastN(db, mId,500)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	for _, el := range *logs{
		response += el.ToString()
	}
	respondPlain(w, http.StatusOK, response)
}

func GetLogsByMachine(db *mgo.Database, w http.ResponseWriter, r *http.Request){
	mId, err := strconv.ParseInt(mux.Vars(r)["m_id"],10, 64)
	logs, err := new(model.Log).FindByMachineLastN(db, mId,500)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	respondJSON(w, http.StatusOK, logs)
}

func GetLogsByMachineAll(db *mgo.Database, w http.ResponseWriter, r *http.Request){
	mId, err := strconv.ParseInt(mux.Vars(r)["m_id"],10, 64)
	logs, err := new(model.Log).FindByMachine(db, mId)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	respondJSON(w, http.StatusOK, logs)
}

func PostLog(db *mgo.Database, w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var log model.Log
	err := decoder.Decode(&log)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	log.Timestamp = time.Now().Unix()
	log.ID = bson.NewObjectId()
	err = log.Insert(db)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}



func NotImplemented(w http.ResponseWriter, r *http.Request){
	w.WriteHeader(http.StatusNotImplemented)
	w.Write([]byte("Not implemented"))
}
