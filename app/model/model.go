package model

import (
	"fmt"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Log struct {
	ID			bson.ObjectId	`bson:"_id" json:"id"`
	Url			string			`bson:"url" json:"url"`
	Method		string			`bson:"method" json:"method"`
	UserAgent	string			`bson:"user_agent" json:"user_agent"`
	IP			string			`bson:"ip" json:"ip"`
	Timestamp	int64			`bson:"timestamp" json:"timestamp"`
	MachineID	int64			`bson:"machine_id" json:"machine_id"`
}

const LogCollectionName = "logs"

func (l *Log) FindAll(db *mgo.Database) (*[]Log, error) {
	var logs []Log
	err := db.C(LogCollectionName).Find(bson.M{}).Sort("-timestamp").All(&logs)
	return &logs, err
}

func (l *Log) FindLastN(db *mgo.Database, n int) (*[]Log, error) {
	var logs []Log
	err := db.C(LogCollectionName).Find(bson.M{}).Limit(n).Sort("-timestamp").All(&logs)
	return &logs, err
}

func (l *Log) FindByMachineLastN(db *mgo.Database, mId int64, n int) (*[]Log, error) {
	var logs []Log
	err := db.C(LogCollectionName).Find(bson.M{"machine_id": mId}).Limit(n).Sort("-timestamp").All(&logs)
	return &logs, err
}

func (l *Log) FindByMachine(db *mgo.Database, mId int64) (*[]Log, error) {
	var logs []Log
	err := db.C(LogCollectionName).Find(bson.M{"machine_id": mId}).Sort("-timestamp").All(&logs)
	return &logs, err
}

func (l *Log) Insert(db *mgo.Database) (error) {
	err := db.C(LogCollectionName).Insert(&l)
	return err
}

func (l *Log) ToString() string {
	return fmt.Sprintf("[%s] IP:%s U-A:\"%s\" %s %s\n", time.Unix(l.Timestamp, 0).Format(time.RFC1123),
		l.IP, l.UserAgent, l.Method, l.Url)
}
