package app

import (
	"net/http"
)

func checkAuth(a *App, h http.HandlerFunc) (http.HandlerFunc) {
	return func(w http.ResponseWriter, r *http.Request) {
		user, pass, _ := r.BasicAuth()
		req, err := http.NewRequest("GET", a.Config.AuthUrl, nil)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}
		req.SetBasicAuth(user, pass)
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		} else if resp.StatusCode != http.StatusOK {
			w.WriteHeader(resp.StatusCode)
			w.Write([]byte(resp.Status))
			return
		} else {
			h.ServeHTTP(w, r)
		}
	}
}

func setupResponseCORS(a *App, h http.HandlerFunc) (http.HandlerFunc) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization")
		h.ServeHTTP(w, r)
	}
}

func setupResponseCTPlain(a *App, h http.HandlerFunc) (http.HandlerFunc) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/plain")
		h.ServeHTTP(w, r)
	}
}

func setupResponseCTJSON(a *App, h http.HandlerFunc) (http.HandlerFunc) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		h.ServeHTTP(w, r)
	}
}