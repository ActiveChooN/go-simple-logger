package app

import (
	"log"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"

	"local/go-simple-logger/app/handler"
	"local/go-simple-logger/config"
)

type App struct {
	Router *mux.Router
	db *mgo.Database
	Config *config.Config
}

func (a *App) Initialize(config *config.Config) {
	a.Config = config
	session, err := mgo.Dial(config.DB.Server)
	if err != nil {
		log.Fatal(err)
	}
	a.db = session.DB(config.DB.Database)
	a.Router = mux.NewRouter()
	a.SetRouter()
}

func (a *App) SetRouter() {
	a.Get("/", a.GetRoot)
	a.Get("/logs/", a.UseMiddleware(a.GetLogs, setupResponseCORS, setupResponseCTJSON, checkAuth))
	a.Get("/logs/plain/", a.UseMiddleware(a.GetLogsPlain, setupResponseCORS, setupResponseCTPlain, checkAuth))
	a.Get("/logs/all/", a.UseMiddleware(a.GetLogsAll, setupResponseCORS, setupResponseCTJSON, checkAuth))
	a.Get("/logs/{m_id:[0-9]+}/", a.UseMiddleware(a.GetLogsByMachine, setupResponseCORS, setupResponseCTJSON, checkAuth))
	a.Get("/logs/{m_id:[0-9]+}/plain/", a.UseMiddleware(a.GetLogsByMachinePlain, setupResponseCORS, setupResponseCTPlain, checkAuth))
	a.Get("/logs/{m_id:[0-9]+}/all/", a.UseMiddleware(a.GetLogsByMachineAll, setupResponseCORS, setupResponseCTJSON, checkAuth))
	a.Post("/logs/", a.UseMiddleware(a.PostLog, checkAuth))
	a.SetupOptionHandler()

}

// Get wraps the router for GET method
func (a *App) Get(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("GET")
}

// Post wraps the router for POST method
func (a *App) Post(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("POST")
}

// Put wraps the router for PUT method
func (a *App) Put(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("PUT")
}

// Delete wraps the router for DELETE method
func (a *App) Delete(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("DELETE")
}

func (a *App) UseMiddleware(h http.HandlerFunc, middleware ...func(a *App, h http.HandlerFunc) http.HandlerFunc) http.HandlerFunc {
	for _, m := range middleware {
		h = m(a, h)
	}
	return h
}

func (a *App) SetupOptionHandler()  {
	a.Router.PathPrefix("/").HandlerFunc(setupResponseCORS(a, func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Allow", "POST, GET, OPTIONS")
	})).Methods("OPTIONS")
}

// Run the app on it's router
func (a *App) Run() {
	fmt.Printf("Application is running on \"%s\"\n", a.Config.Host)
	log.Fatal(http.ListenAndServe(a.Config.Host, a.Router))
}

func (a *App) GetLogsPlain(w http.ResponseWriter, r *http.Request)  {
	handler.GetLogsPlain(a.db, w, r)
}

func (a *App) PostLog(w http.ResponseWriter, r *http.Request)  {
	handler.PostLog(a.db, w, r)
}

func (a *App) GetRoot(w http.ResponseWriter, r *http.Request)  {
	handler.GetRoot(w, r)
}

func (a *App) GetLogsByMachinePlain(w http.ResponseWriter, r *http.Request)  {
	handler.GetLogsByMachinePlain(a.db, w, r)
}

func (a *App) GetLogs(w http.ResponseWriter, r *http.Request)  {
	handler.GetLogs(a.db, w, r)
}

func (a *App) GetLogsByMachine(w http.ResponseWriter, r *http.Request) {
	handler.GetLogsByMachine(a.db, w, r)
}

func (a *App) GetLogsAll(w http.ResponseWriter, r *http.Request)  {
	handler.GetLogsAll(a.db, w, r)
}

func (a *App) GetLogsByMachineAll(w http.ResponseWriter, r *http.Request)  {
	handler.GetLogsByMachineAll(a.db, w, r)
}